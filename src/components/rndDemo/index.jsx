import React from "react";
import { Rnd } from "react-rnd";
import styled from "styled-components";

// components
import { widthWithGutterSpace, heightWithGutterSpace } from "utils/gridValues";

// assets
import { Square } from "assets/icons";
import { get } from "lodash";

export default function RndDemo(props) {
  let {
    isShowBorder,
    isShowGrid,
    showGridHandler,
    width,
    height,
    onDragHandler,
    onDragStartHandler,
    onDragStopHandler,
    onResizeStopHandler,
    onResizeHandler,
    isResizing,
    onResizeStartHandler,
    x,
    y,
    children,
    classNameText,
    showResizingHandler,
    enableResizing,
    elementDetails,
    activeElement,
    handleStateChange,
    isDragging,
  } = props;
  const isCurrentElemActive =
    get(activeElement, "_id") === get(elementDetails, "_id");
  return (
    <RndContainer
      isResizing={isResizing}
      isShowGrid={isShowGrid}
      isShowBorder={isShowBorder}
      isCurrentElemActive={isCurrentElemActive}
    >
      <Rnd
        className="rnd-wrapper"
        size={{ width, height }}
        position={{ x, y }}
        minWidth={50}
        minHeight={50}
        bounds={`.${classNameText}`}
        dragGrid={[widthWithGutterSpace, heightWithGutterSpace]}
        onResizeStart={onResizeStartHandler}
        onResize={onResizeHandler}
        onResizeStop={onResizeStopHandler}
        onDrag={onDragHandler}
        onDragStart={onDragStartHandler}
        onDragStop={onDragStopHandler}
        onClick={() => handleStateChange("activeElement", elementDetails)}
        // onMouseOver={() => handleStateChange("activeElement", elementDetails)}
        resizeHandleComponent={{
          top: <SquareIcon />,
          bottom: <SquareIcon />,
          right: <SquareIcon />,
          left: <SquareIcon />,
          topLeft: <SquareIcon />,
          topRight: <SquareIcon />,
          bottomLeft: <SquareIcon />,
          bottomRight: <SquareIcon />,
        }}
        enableResizing={isCurrentElemActive && !isDragging}
      >
        {children}
      </Rnd>
    </RndContainer>
  );
}

const SquareIcon = styled(Square)`
  position: absolute;
  transform: translate(-50%, -50%);
  width: 100%;
  height: 100%;
  top: 50%;
  left: 50%;
  max-width: 10px;
  max-height: 10px;
`;

const RndContainer = styled.div`
  .rnd-wrapper {
    display: flex;
    align-items: center;
    justify-content: center;
    transition: ${(props) => !props.isResizing && `all 0.1s ease-in`};
    border: solid 2px
      ${(props) => (props.isCurrentElemActive ? "red" : "transparent")};
    &:hover {
      border: solid 2px red;
    }
  }
`;
